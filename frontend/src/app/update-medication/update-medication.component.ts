import {Component, OnInit} from '@angular/core';
import {Medication} from '../models/medication';
import {MedicationService} from '../service/medication.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-update-medication',
  templateUrl: './update-medication.component.html',
  styleUrls: ['./update-medication.component.css']
})
export class UpdateMedicationComponent implements OnInit {

  id: number;
  medication: Medication;
  submitted = false;

  constructor(private route: ActivatedRoute, private router: Router,
              private medicationService: MedicationService) {
  }

  ngOnInit() {
    this.medication = new Medication();

    this.id = this.route.snapshot.params.id;

    this.medicationService.getMedication(this.id)
      .subscribe(data => {
        console.log(data);
        this.medication = data;
      }, error => console.log(error));
  }

  updateMedication() {
    console.log(this.medication);
    this.medicationService.updateMedication(this.id, this.medication)
      .subscribe(
        data => {
          console.log(data);
          alert('Medication was successfully updated');
        },
        error => console.log(error));
    this.medication = new Medication();
    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.updateMedication();
  }

  gotoList() {
    this.router.navigate(['doctor']);
  }

  back() {
    this.router.navigate(['/doctor/medication']);
  }

}
