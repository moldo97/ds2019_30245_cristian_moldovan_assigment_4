import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {REST_API} from '../common/API';
import {User} from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class CaregiverService {

  constructor(private http: HttpClient) { }

  getCaregiver(id: number): Observable<any> {
    return this.http.get(`${REST_API + 'user/caregiver'}/${id}`);
  }

  createCaregiver(user: User): Observable<any> {
    return this.http.post(`${REST_API + 'user/caregiver'}`, user);
  }

  updateCaregiver(id: number, user: User): Observable<any> {
    return this.http.put(`${REST_API + 'user/caregiver'}/${id}`, user);
  }

  deleteCaregiver(id: number): Observable<any> {
    return this.http.delete(`${REST_API + 'user/caregiver'}/${id}`, {responseType: 'text'});
  }

  getCaregiverList(): Observable<any> {
    return this.http.get(`${REST_API + 'user/caregiver'}`);
  }
}
