import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {REST_API} from '../common/API';
import {LoginDTO} from '../models/user';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {
  }

  login(user: LoginDTO): Observable<any> {
    return this.http.post(`${REST_API + 'user/login'}`, user);
  }

  getUser(username: string): Observable<any> {
    return this.http.get(`${REST_API + 'user'}/${username}`);
  }

  getActivities(id: number): Observable<any> {
    return this.http.get(`${REST_API + 'user/activities'}/${id}`);
  }

  getMedicationPlans(id: number): Observable<any> {
    return this.http.get(`${REST_API + 'user/medicationPlans'}/${id}`);
  }

  setNormal(id: number, normal: boolean): Observable<any> {
    const body = JSON.stringify({normal});
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.post(`${REST_API + 'user/setNormal'}/${id}`, body,{headers});
  }

  setRecommendation(id: number, recommendation: string): Observable<any> {
    const body = JSON.stringify({recommendation});
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.post(`${REST_API + 'user/setRecommendation'}/${id}`, body, {headers});
  }
}
