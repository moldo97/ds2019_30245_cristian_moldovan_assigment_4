import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {REST_API} from '../common/API';
import {Patient} from '../models/patient';

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  constructor(private http: HttpClient) {
  }

  getPatientList(username: string): Observable<any> {
    return this.http.get(`${REST_API + 'patient/get'}/${username}`);
  }

  getPatientDoctorList(username: string): Observable<any> {
    return this.http.get(`${REST_API + 'patient/getDoctor'}/${username}`);
  }

  getPatient(id: number): Observable<any> {
    return this.http.get(`${REST_API + '/patient'}/${id}`);
  }

  createPatient(patient: Patient): Observable<any> {
    return this.http.post(`${REST_API + '/patient'}`, patient);
  }

  updatePatient(patient: Patient, id: number): Observable<any> {
    return this.http.put(`${REST_API + '/patient'}/${id}`, patient);
  }

  deletePatient(id: number): Observable<any> {
    return this.http.delete(`${REST_API + '/patient'}/${id}`, {responseType: 'text'});
  }
}
