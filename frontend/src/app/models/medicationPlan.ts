export class MedicationPlan {
  nameMed: string;
  startTime: Date;
  endTime: Date;
  intakeIntervals: string;
  taken: string;
}
