export class Medication {
  id: number;
  dossage: number;
  name: string;
  sideEffects: string;
 }
