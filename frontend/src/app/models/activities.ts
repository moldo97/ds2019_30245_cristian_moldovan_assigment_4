export class Activities {
  id: number;
  name: string;
  startTime: Date;
  endTime: Date;
  behavior: boolean;
  recomandare: string;
}
