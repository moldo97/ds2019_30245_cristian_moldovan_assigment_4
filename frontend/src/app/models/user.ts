export class LoginDTO {
  username: string;
  password: string;
}

export class User {
  id: number;
  username: string;
  password: string;
  role: string;
  name: string;
  birthDate: Date;
  address: string;
  gender: string;
}
