import {Component, OnInit} from '@angular/core';
import {User} from '../models/user';
import {ActivatedRoute, Router} from '@angular/router';
import {CaregiverService} from '../service/caregiver.service';
import {Patient} from '../models/patient';
import {PatientService} from '../service/patient.service';
import {UserService} from '../service/user.service';
import {Observable} from 'rxjs';
import {Activities} from '../models/activities';
import {MedicationPlan} from '../models/medicationPlan';

@Component({
  selector: 'app-patient-details',
  templateUrl: './patient-details.component.html',
  styleUrls: ['./patient-details.component.css']
})
export class PatientDetailsComponent implements OnInit {

  id: number;
  patient: Patient;

  activities: Activities[];
  medicationPlans: MedicationPlan[];

  constructor(private route: ActivatedRoute, private router: Router,
              private patientService: PatientService,
              private caregiverService: CaregiverService,
              private userService: UserService) {
  }

  ngOnInit() {
    this.patient = new Patient();

    this.id = this.route.snapshot.params.id;

    this.patientService.getPatient(this.id)
      .subscribe(data => {
        console.log(data);
        this.patient = data;
      }, error => console.log(error));

    this.userService.getActivities(this.id)
      .subscribe(data => {
        console.log(data);
        this.activities = data;
      }, error => console.log(error));

    this.userService.getMedicationPlans(this.id)
      .subscribe(data => {
        console.log(data);
        this.medicationPlans = data;
      }, error => console.log(error));
  }

  list() {
    this.router.navigate(['doctor/patient']);
  }

  setNormal(idActivity: any, normal: boolean) {
    this.userService.setNormal(idActivity, normal)
      .subscribe(data => {
        console.log(data);
      }, error => console.log(error));
  }

  addRecomandation(idActivity: any, recomandation: string) {
    this.userService.setRecommendation(idActivity, recomandation)
      .subscribe(data => {
        console.log(data);
      }, error => console.log(error));
  }

}
