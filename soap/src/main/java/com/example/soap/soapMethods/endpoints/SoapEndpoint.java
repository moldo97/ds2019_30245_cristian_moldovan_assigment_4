package com.example.soap.soapMethods.endpoints;


//import com.example.soap.service.GetStudentDetailsRequest;
//import com.example.soap.service.GetStudentDetailsResponse;
//import com.example.soap.service.StudentDetails;
import com.example.soap.model.Activity;
import com.example.soap.model.MedicationPerPlan;
import com.example.soap.repository.ActivityRepository;
//import com.example.soap.service.ActivityDetails;
////import com.example.soap.service.GetActivityDetailsRequest;
////import com.example.soap.service.GetActivityDetailsResponse;
import com.example.soap.repository.MedicationPerPlanRepository;
import com.example.soap.repository.MedicationPlanRepository;
import com.example.soap.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

@Endpoint
public class SoapEndpoint {


    private ActivityRepository activityRepository;
    private MedicationPerPlanRepository medicationPerPlanRepository;
    private MedicationPlanRepository medicationPlanRepository;

    @Autowired
    public SoapEndpoint(ActivityRepository activityRepository
            , MedicationPerPlanRepository medicationPerPlanRepository
            , MedicationPlanRepository medicationPlanRepository) {

        this.activityRepository = activityRepository;
        this.medicationPerPlanRepository = medicationPerPlanRepository;
        this.medicationPlanRepository = medicationPlanRepository;
    }

    //    @PayloadRoot(namespace = "service", localPart = "GetStudentDetailsRequest")
//    @ResponsePayload
//    public GetStudentDetailsResponse processCourseDetailsRequest(@RequestPayload GetStudentDetailsRequest request) {
//        GetStudentDetailsResponse response = new GetStudentDetailsResponse();
//        StudentDetails studentDetails = new StudentDetails();
//        studentDetails.setId(request.getId());
//        studentDetails.setName("Adam");
//        studentDetails.setPassportNumber("E1234567");
//        response.setStudentDetails(studentDetails);
//        return response;
//    }

    @PayloadRoot(namespace = "service", localPart = "GetActivityDetailsRequest")
    @ResponsePayload
    public GetActivityDetailsResponse getActivities(@RequestPayload GetActivityDetailsRequest request){
        GetActivityDetailsResponse response = new GetActivityDetailsResponse();
        List<ActivityDetails> activityDetails = response.getActivityDetails();
        List<Activity> activities = activityRepository.findAllByPatientId(request.getId());
        for(Activity activity: activities){
            ActivityDetails activityDetails1 = new ActivityDetails();
            activityDetails1.setId(activity.getId());
            activityDetails1.setBehavior(activity.getNormal());
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
            activityDetails1.setStartTime(formatter.format(activity.getStartTime()));
            activityDetails1.setEndTime(formatter.format(activity.getEndTime()));
            activityDetails1.setName(activity.getActivity());
            activityDetails.add(activityDetails1);
        }

        return response;
    }

    @PayloadRoot(namespace = "service", localPart = "GetMedicationPlanDetailsRequest")
    @ResponsePayload
    public GetMedicationPlanDetailsResponse getMedicationPlans(@RequestPayload GetMedicationPlanDetailsRequest request){
        GetMedicationPlanDetailsResponse response = new GetMedicationPlanDetailsResponse();
        List<MedicationPlan> medicationPlans = response.getMedicationPlan();
        List<com.example.soap.model.MedicationPlan> medicationPlansModel = medicationPlanRepository.findAllByPatientId(request.getPatientId());

        for(com.example.soap.model.MedicationPlan medicationPlan : medicationPlansModel) {
            List<MedicationPerPlan> medicationPlansDetails = medicationPerPlanRepository.findAllByMedicationPlanId(medicationPlan.getId());
            for(MedicationPerPlan medicationPerPlan : medicationPlansDetails){
                MedicationPlan medicationPlan1 = new MedicationPlan();
                medicationPlan1.setNameMed(medicationPerPlan.getMedication().getName());
                medicationPlan1.setIntakeIntervals(medicationPerPlan.getIntake_intervals());
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
                medicationPlan1.setStartTime(formatter.format(medicationPerPlan.getStartTime()));
                medicationPlan1.setEndTime(formatter.format(medicationPerPlan.getStartTime()));
                medicationPlan1.setTaken(medicationPerPlan.getTaken());
                medicationPlans.add(medicationPlan1);
            }
        }

        return response;
    }

    @PayloadRoot(namespace = "service", localPart = "SetNormalRequest")
    @ResponsePayload
    public SetNormalResponse setNormal(@RequestPayload SetNormalRequest request){
        SetNormalResponse response = new SetNormalResponse();
        Optional<Activity> activity = activityRepository.findById(request.getIdActivity());

        if(!activity.isPresent()){
            response.setMessage("Pacient necunoscut");
            return response;
        }

        Activity activity1 = activity.get();
        activity1.setNormal(request.isSetNormal());

        activityRepository.save(activity1);

        response.setMessage("Succes");

        return response;
    }

    @PayloadRoot(namespace = "service", localPart = "SetRecommendationRequest")
    @ResponsePayload
    public SetRecommendationResponse setNormal(@RequestPayload SetRecommendationRequest request){
        SetRecommendationResponse response = new SetRecommendationResponse();
        Optional<Activity> activity = activityRepository.findById(request.getIdActivity());

        if(!activity.isPresent()){
            response.setMessage("Pacient necunoscut");
            return response;
        }

        Activity activity1 = activity.get();
        activity1.setRecommendation(request.getSetRecommendation());

        activityRepository.save(activity1);

        response.setMessage("Succes");

        return response;
    }
}
