package com.example.soap.repository;

import com.example.soap.model.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicationPlanRepository extends JpaRepository<MedicationPlan,Long> {
    List<MedicationPlan> findAllByPatientId(Long id);
}
