package com.example.soap.repository;

import com.example.soap.model.MedicationPerPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicationPerPlanRepository extends JpaRepository<MedicationPerPlan,Long> {
    List<MedicationPerPlan> findAllByMedicationPlanId(Long id);
}
