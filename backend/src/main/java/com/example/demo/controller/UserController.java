package com.example.demo.controller;

import com.example.demo.dto.*;
import com.example.demo.service.SOAPService;
import com.example.demo.service.UserService;
import com.example.demo.soap.ActivityDetails;
import com.example.demo.soap.MedicationPlan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins =  "http://localhost:4200")
@RequestMapping(value = "/user")
public class UserController {

    private final UserService userService;

    private final SOAPService soapService;

    @Autowired
    public UserController(UserService userService, SOAPService soapService) {
        this.userService = userService;
        this.soapService= soapService;
    }

    //Caregiver role
    //////////////////////////////////////////////////////////////

    @GetMapping(value = "/caregiver/{id}")
    public CaregiverDTO findCaregiverById(@PathVariable("id") Long id) { return userService.findUserById(id); }

    @GetMapping(value = "/{username}")
    public CaregiverViewDTO findUserByUsername(@PathVariable("username") String username) { return userService.findUserByUsername(username); }

    @GetMapping(value = "/caregiver")
    public List<CaregiverDTO> findAllCaregiver(){
        return userService.findAllCaregiver();
    }

    @PostMapping(value = "/login")
    public LoginDTO login(@RequestBody LoginDTO loginDTO) { return userService.login(loginDTO); }

    @PostMapping(value = "/caregiver")
    public Long createCaregiver(@RequestBody CaregiverDTO caregiverDTO){
        return userService.create(caregiverDTO);
    }

    @PutMapping(value = "/caregiver/{id}")
    public Long updateCaregiver(@RequestBody CaregiverViewDTO caregiverViewDTO,  @PathVariable("id") Long id){
        return userService.update(caregiverViewDTO,id);
    }

    @DeleteMapping(value = "/caregiver/{id}")
    public Map<String, Boolean> deleteCaregiver(@PathVariable("id") Long id){
        return userService.delete(id);
    }


    @GetMapping(value = "/activities/{id}")
    public List<ActivityDetails> getActivities(@PathVariable("id") Long id) { return soapService.getActivities(id); }

    @GetMapping(value = "/medicationPlans/{id}")
    public List<MedicationPlan> getMedicationsPlans(@PathVariable("id") Long id){ return soapService.getMedicationsPlans(id); }
    //////////////////////////////////////////////////////////////

    @PostMapping(value = "/setNormal/{id}")
    public MessageDTO setNormal(@PathVariable("id") Long id,@RequestBody NormalDTO normal){
        return new MessageDTO(soapService.setNormal(id,normal.getNormal()));
    }

    @PostMapping(value = "/setRecommendation/{id}")
    public MessageDTO setRecommendation(@PathVariable("id") Long id,@RequestBody RecommendationDTO reccomendation){
        return  new MessageDTO(soapService.setRecommendation(id,reccomendation.getRecommendation()));
    }
}
