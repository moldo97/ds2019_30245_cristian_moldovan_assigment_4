package com.example.demo;

import com.example.demo.service.DispenserService;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.io.IOException;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {

		ApplicationContext context =SpringApplication.run(DemoApplication.class, args);
		try {
			Server server = ServerBuilder.forPort(8088)
					.addService(context.getBean(DispenserService.class))
					.build()
					.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
