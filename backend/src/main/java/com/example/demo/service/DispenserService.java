package com.example.demo.service;

import com.example.demo.grpc.DispenserGrpc;
import com.example.demo.grpc.MedicationPlanRequest;
import com.example.demo.grpc.MedicationPlanResponse;
import com.example.demo.model.MedicationPerPlan;
import com.example.demo.repository.MedicationPerPlanRepository;
import io.grpc.stub.StreamObserver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class DispenserService extends DispenserGrpc.DispenserImplBase {

    MedicationPerPlanRepository medicationPerPlanRepository;

    @Autowired
    public DispenserService(MedicationPerPlanRepository medicationPerPlanRepository) {
        this.medicationPerPlanRepository = medicationPerPlanRepository;
    }

    @Override
    public void getMedicationPlans(MedicationPlanRequest medicationPlanRequest
            , StreamObserver<MedicationPlanResponse> medicationPlanResponseStreamObserver){
        List<MedicationPerPlan> medicationPerPlanList = medicationPerPlanRepository.findAll();
        Date currentDate= new Date(System.currentTimeMillis());
        for(MedicationPerPlan medicationPerPlan : medicationPerPlanList){
            if( currentDate.before(medicationPerPlan.getEndTime())
                    && currentDate.after(medicationPerPlan.getStartTime())){
                String pattern = "MM/dd/yyyy";
                DateFormat df = new SimpleDateFormat(pattern);
                String start_end = df.format(medicationPerPlan.getStartTime()) + " - "
                        + df.format(medicationPerPlan.getEndTime());

                String[] intake = medicationPerPlan.getIntake_intervals().split("-");


                MedicationPlanResponse med = MedicationPlanResponse.newBuilder().setStartEndTime(start_end)
                        .setMedicationName(medicationPerPlan.getMedication().getName())
                        .setPatientName(medicationPerPlan.getMedicationPlan().getPatient().getName())
                        .setIntakeStart(intake[0])
                        .setIntakeEnd(intake[1])
                        .build();

                medicationPlanResponseStreamObserver.onNext(med);
            }
        }
        medicationPlanResponseStreamObserver.onCompleted();
    }
}
