package com.example.demo.service;

import com.example.demo.config.SoapConnector;

import com.example.demo.model.MedicationPerPlan;
import com.example.demo.soap.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SOAPService {

    @Autowired
    private SoapConnector connector;

    public List<ActivityDetails> getActivities(Long id){
        GetActivityDetailsRequest request = new GetActivityDetailsRequest();

        request.setId(id);
        GetActivityDetailsResponse response = (GetActivityDetailsResponse) connector.callWebService("http://localhost:8085/doctorservice",request);

        return response.getActivityDetails();
    }

    public List<MedicationPlan> getMedicationsPlans(Long id){
        GetMedicationPlanDetailsRequest request = new GetMedicationPlanDetailsRequest();

        request.setPatientId(id);
        GetMedicationPlanDetailsResponse response = (GetMedicationPlanDetailsResponse) connector.callWebService("http://localhost:8085/doctorservice",request);

        return response.getMedicationPlan();
    }

    public String setNormal(Long activityId, Boolean normal){
        SetNormalRequest request = new SetNormalRequest();

        request.setIdActivity(activityId);
        request.setSetNormal(normal);

        SetNormalResponse response = (SetNormalResponse) connector.callWebService("http://localhost:8085/doctorservice",request);

        return response.getMessage();
    }

    public String setRecommendation(Long activityId, String recommendation){
        SetRecommendationRequest request = new SetRecommendationRequest();

        request.setIdActivity(activityId);
        request.setSetRecommendation(recommendation);

        SetRecommendationResponse response = (SetRecommendationResponse) connector.callWebService("http://localhost:8085/doctorservice",request);

        return response.getMessage();
    }
}
