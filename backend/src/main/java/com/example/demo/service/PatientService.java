package com.example.demo.service;

import com.example.demo.dto.PacientViewDTO;
import com.example.demo.dto.PatientDTO;
import com.example.demo.dto.builders.PacientBuilder;
import com.example.demo.dto.builders.PacientViewBuilder;
import com.example.demo.errorHandler.ResourceNotFoundException;
import com.example.demo.model.Patient;
import com.example.demo.model.User;
import com.example.demo.repository.PatientRepository;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatientService {

    private final PatientRepository patientRepository;

    private final UserRepository userRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository, UserRepository userRepository) {
        this.patientRepository = patientRepository;
        this.userRepository = userRepository;
    }


    //get patients for doctor
    public List<PatientDTO> getAllPatientsByDoctorUsername(String username){
        User user = userRepository.findByUsername(username);
        List<Patient> patients = patientRepository.findByDoctorId(user.getId());

        return patients.stream()
                .map(patient -> PacientBuilder.generateDTOFromEntity(patient))
                .collect(Collectors.toList());
    }

    //get patients for caregiver
    public List<PacientViewDTO> getAllPatientsByCaregiverUsername(String username){
        User user = userRepository.findByUsername(username);
        List<Patient> patients = patientRepository.findByCaregiverId(user.getId());

        return patients.stream()
                .map(patient -> PacientViewBuilder.generateDTOFromEntity(patient))
                .collect(Collectors.toList());
    }

    //get patient by id
    public PatientDTO findPatientById(Long id){
        Optional<Patient> patient = patientRepository.findById(id);

        if(!patient.isPresent()){
            throw new ResourceNotFoundException("Patient","id",id);
        }

        return PacientBuilder.generateDTOFromEntity(patient.get());
    }

    public Long createPatient(PatientDTO patientDTO){
        Optional<User> doctor = userRepository.findById(patientDTO.getDoctorId());
        Optional<User> caregiver = userRepository.findById(patientDTO.getCaregiverId());
        Patient patient = PacientBuilder.generateEntityFromDTO(patientDTO);

        if(!doctor.isPresent()){
            throw new ResourceNotFoundException("Patient","id",patientDTO.getDoctorId());
        }


        if(!caregiver.isPresent()){
            throw new ResourceNotFoundException("Patient","id",patientDTO.getCaregiverId());
        }


        patient.setDoctor(doctor.get());
        patient.setCaregiver(caregiver.get());

        return patientRepository
                .save(patient)
                .getId();
    }

    public Long updatePatient(PatientDTO patientDTO,Long id){
        Optional<Patient> patient = patientRepository.findById(id);
        Optional<User> doctor = userRepository.findById(patientDTO.getDoctorId());
        Optional<User> caregiver = userRepository.findById(patientDTO.getCaregiverId());



        if(!doctor.isPresent()){
            throw new ResourceNotFoundException("Doctor","id",patientDTO.getDoctorId());
        }


        if(!caregiver.isPresent()){
            throw new ResourceNotFoundException("Caregiver","id",patientDTO.getCaregiverId());
        }

        if(!patient.isPresent()){
            throw new ResourceNotFoundException("Patient","id",patientDTO.getId());
        }

        Patient patientUpdate = PacientBuilder.generateEntityFromDTO(patientDTO);
        patientUpdate.setId(id);
        patientUpdate.setDoctor(doctor.get());
        patientUpdate.setCaregiver(caregiver.get());
        return patientRepository
                .save(patientUpdate)
                .getId();
    }

    public Map<String, Boolean> deletePatient(Long id){
        Patient patient = patientRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Patient","id",id));

        patientRepository.delete(patient);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
