package com.example.demo.dto.builders;

import com.example.demo.dto.ActivityDTO;
import com.example.demo.model.Activity;

public class ActivityBuilder {

    private ActivityBuilder(){

    }

    public static Activity generateEntityFromDTO(ActivityDTO activityDTO){
        return new Activity(activityDTO.getId(),
                activityDTO.getStartTime(),
                activityDTO.getEndTime(),
                activityDTO.getActivity(),
                activityDTO.getNormal());
    }
}
