package com.example.demo.dto;

import com.example.demo.enums.Gender;
import com.example.demo.enums.Role;

import java.util.Date;

public class CaregiverViewDTO {
    private Long id;
    private Role role;
    private String username;
    private String password;
    private String name;
    private Date birthDate;
    private String address;
    private Gender gender;

    public CaregiverViewDTO(Long id, Role role, String username, String password, String name, Date birthDate, String address, Gender gender) {
        this.role = role;
        this.username = username;
        this.password = password;
        this.name = name;
        this.birthDate = birthDate;
        this.address = address;
        this.gender = gender;
        this.id = id;
    }

    public CaregiverViewDTO(){

    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
