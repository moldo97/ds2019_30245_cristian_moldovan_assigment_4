package com.example.demo.dto;

public class NormalDTO {
    private Boolean normal;

    public NormalDTO() {
    }

    public NormalDTO(Boolean normal) {
        this.normal = normal;
    }

    public Boolean getNormal() {
        return normal;
    }

    public void setNormal(Boolean normal) {
        this.normal = normal;
    }
}
