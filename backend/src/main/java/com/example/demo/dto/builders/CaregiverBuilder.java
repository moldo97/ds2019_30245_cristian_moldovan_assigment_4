package com.example.demo.dto.builders;

import com.example.demo.dto.CaregiverDTO;
import com.example.demo.model.User;

public class CaregiverBuilder {

    private CaregiverBuilder(){

    }

    public static CaregiverDTO generateDTOFromEntity(User user){
        return new CaregiverDTO(user.getId(),
                user.getUsername(),
                user.getPassword(),
                user.getRole(),
                user.getName(),
                user.getBirthDate(),
                user.getAdress(),
                user.getGender());
    }

    public static User generateEntityFromDto(CaregiverDTO caregiverDTO){
        return new User(caregiverDTO.getId(),
                caregiverDTO.getUsername(),
                caregiverDTO.getPassword(),
                caregiverDTO.getRole(),
                caregiverDTO.getName(),
                caregiverDTO.getBirthDate(),
                caregiverDTO.getAddress(),
                caregiverDTO.getGender());
    }
}
