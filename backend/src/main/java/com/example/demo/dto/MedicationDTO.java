package com.example.demo.dto;

public class MedicationDTO {
    private Long id;
    private String name;
    private String sideEffects;
    private Double dossage;

    public MedicationDTO(){

    }

    public MedicationDTO(Long id, String name, String sideEffects, Double dossage) {
        this.id = id;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dossage = dossage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public Double getDossage() {
        return dossage;
    }

    public void setDossage(Double dossage) {
        this.dossage = dossage;
    }
}
