package com.example.demo.dto;

public class RecommendationDTO {
    private String recommendation;

    public RecommendationDTO(String recommendation) {
        this.recommendation = recommendation;
    }

    public RecommendationDTO() {
    }

    public String getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }
}
