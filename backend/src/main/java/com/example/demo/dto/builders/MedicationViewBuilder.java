package com.example.demo.dto.builders;

import com.example.demo.dto.MedicationViewDTO;
import com.example.demo.model.Medication;

public class MedicationViewBuilder {

    public static MedicationViewDTO generateDTOFromEntity(Medication medication){
        return new MedicationViewDTO(medication.getName(),
                medication.getSideEffects(),
                medication.getDosage());
    }

    public static Medication generateEntityFromDTO(MedicationViewDTO medicationViewDTO){
        return new Medication(medicationViewDTO.getName(),
                medicationViewDTO.getSideEffects(),
                medicationViewDTO.getDossage());
    }
}
